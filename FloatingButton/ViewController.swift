//
//  ViewController.swift
//  FloatingButton
//
//  Created by BCA-GSIT-iMAC-11 on 26/01/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var WelcomeImageView: UIImageView!
    @IBOutlet var OptionCollectionView: UICollectionView!
    @IBOutlet var MovieListCollectionView: UICollectionView!
    
    private let floatingButton: UIButton = {
       let button = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 45
        let image = UIImage(named: "SaleButton")
        button.setImage(image, for: .normal)
        button.backgroundColor = UIColor(red: 185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 0.6)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.addSubview(floatingButton)
        floatingButton.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(moveButton)))
        
        setupImageView()
        setupOptionsCollectionViewCell()
        setupMoviesListCollectionViewCell()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        floatingButton.frame = CGRect(x: view.frame.size.width - 100, y: view.frame.size.height - 188, width: 90, height: 90)
    }
    
    @objc func moveButton(gesture: UIPanGestureRecognizer) {
        //print(gesture.location(in: self.view))
        let location = gesture.location(in: self.view)
        let buttonPosition = gesture.view
        buttonPosition?.center = location
        
        if gesture.state == .ended {
            if self.floatingButton.frame.midX <= self.view.layer.frame.width / 2 {
                self.floatingButton.center.x = 45
                if self.floatingButton.center.y <= 89 {
                    self.floatingButton.center.y = 90
                }
            }
            else {
                self.floatingButton.center.x = self.view.layer.frame.width - 45
                if self.floatingButton.center.y <= 89 {
                    self.floatingButton.center.y = 90
                }
            }
        }
    }
    
    func setupImageView() {
        WelcomeImageView.image = UIImage(named: "HelloImage")
        WelcomeImageView.contentMode = .scaleAspectFill
    }
    
    func setupOptionsCollectionViewCell() {
        OptionCollectionView.dataSource = self
        //OptionCollectionView.delegate = self
        //OptionCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 25
        layout.itemSize = CGSize(width: 60, height: 60)
        self.OptionCollectionView.collectionViewLayout = layout
        self.OptionCollectionView.backgroundColor = UIColor(patternImage: UIImage(named: "MoviesListBackground")!)
    }
    
    func setupMoviesListCollectionViewCell() {
        MovieListCollectionView.dataSource = self
        
        let layout2 = UICollectionViewFlowLayout()
        layout2.sectionInset = UIEdgeInsets(top: 35, left: 45, bottom: 0, right: 75)
        layout2.minimumLineSpacing = 10
        layout2.minimumInteritemSpacing = 25
        layout2.itemSize = CGSize(width: 125, height: 190)
        self.MovieListCollectionView.collectionViewLayout = layout2
        self.MovieListCollectionView.backgroundColor = UIColor(patternImage: UIImage(named: "MoviesListBackground")!)
    }

}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == OptionCollectionView {
            return options.count
        }
        else if collectionView == MovieListCollectionView {
            return movies.count
        }
        else {
            return movies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == OptionCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionViewCell", for: indexPath) as! IconCollectionViewCell
            cell.setupCell(option: options[indexPath.row])
            cell.contentView.layer.cornerRadius = 10
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
            cell.setupMoviesCell(movie: movies[indexPath.row])
            //cell.contentView.layer.cornerRadius = 10
            return cell
        }
    }
    
}

/*extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }

}*/
