//
//  Option.swift
//  FloatingButton
//
//  Created by BCA-GSIT-iMAC-11 on 27/01/22.
//

import Foundation
import UIKit

struct Option {
    let text: String
}

let options: [Option] = [
    Option(text: "Buy"),
    Option(text: "Sell"),
    Option(text: "Rent"),
    Option(text: "Return"),
    Option(text: "Extend"),
    Option(text: "Preview")
]
