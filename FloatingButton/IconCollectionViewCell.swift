//
//  IconCollectionViewCell.swift
//  FloatingButton
//
//  Created by BCA-GSIT-iMAC-11 on 27/01/22.
//

import UIKit

class IconCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var OptionLabel: UIButton!
    
    func setupCell(option: Option) {
        OptionLabel.setTitle("\(option.text)", for: .normal)
        OptionLabel.tintColor = .black
    }
    
}
