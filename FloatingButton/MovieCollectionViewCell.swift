//
//  MovieCollectionViewCell.swift
//  FloatingButton
//
//  Created by BCA-GSIT-iMAC-11 on 27/01/22.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var MoviePosterImageView: UIImageView!
    @IBOutlet var MovieTitleLabel: UILabel!
    
    func setupMoviesCell(movie: Movie) {
        MoviePosterImageView.image = movie.poster
        MovieTitleLabel.text = "\(movie.title)"
    }
}
