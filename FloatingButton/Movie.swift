//
//  Movie.swift
//  FloatingButton
//
//  Created by BCA-GSIT-iMAC-11 on 27/01/22.
//

import Foundation
import UIKit

struct Movie {
    let poster: UIImage
    let title: String
}

let movies: [Movie] = [
    Movie(poster: UIImage(named: "The Avengers Poster")!, title: "The Avengers"),
    Movie(poster: UIImage(named: "Avengers Endgame Poster")!, title: "Avengers Endgame"),
    Movie(poster: UIImage(named: "Aladdin Poster")!, title: "Aladdin"),
    Movie(poster: UIImage(named: "Beauty and The Beast Poster")!, title: "Beauty and The Beast"),
    Movie(poster: UIImage(named: "Encanto Poster")!, title: "Encanto"),
    Movie(poster: UIImage(named: "Raya Poster")!, title: "Raya and The Last Dragon"),
    Movie(poster: UIImage(named: "Luca Poster")!, title: "Luca"),
    Movie(poster: UIImage(named: "Frozen Poster")!, title: "Frozen")
]
